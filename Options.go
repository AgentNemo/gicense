package gicense

import (
	"gitlab.com/AgentNemo/gicense/encryption"
	"gitlab.com/AgentNemo/gicense/secret"
	"gitlab.com/AgentNemo/gicense/signing"
)

// Default ISigning Method
func DefaultSigning(option *LicenseGenerator) {
	HMAC := signing.NewHMAC(signing.WEAK)
	option.Signing = HMAC
}

// Default Secret Method
func DefaultSecret(option *LicenseGenerator) {
	hash := secret.NewHash()
	option.Secret = hash
}

// Default IEncryption Method
func DefaultEncryption(option *LicenseGenerator) {
	aes := encryption.AES{}
	option.Encryption = &aes
	DefaultSecret(option)
}
