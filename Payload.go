package gicense

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/joomcode/errorx"
	"time"
)

// https://tools.ietf.org/html/rfc7519#section-4.1

var (
	ErrorsPayload        = errorx.NewNamespace("Payload")
	ErrorPayloadNotValid = errorx.NewType(ErrorsPayload, "NotValid")
)

type Payload struct {
	jwt.StandardClaims
	Data map[string]interface{}
}

// Creates a new payload
func NewPayload(id string, from string, to string, product string, end time.Time, additional map[string]interface{}) *Payload {
	return &Payload{StandardClaims: jwt.StandardClaims{
		Id:        id,
		Audience:  to,
		ExpiresAt: end.Unix(),
		IssuedAt:  time.Now().Unix(),
		Issuer:    from,
		Subject:   product,
		NotBefore: time.Now().Unix(),
	}, Data: additional}
}

// Sets the the start time the token starts to be valid
func (p *Payload) StartAt(start time.Time) {
	p.NotBefore = start.Unix()
}

// Check if the Payload is Valid
func (p *Payload) IsValid() error {
	err := p.StandardClaims.Valid()
	if err != nil {
		return ErrorPayloadNotValid.WrapWithNoMessage(err)
	}
	return nil
}
