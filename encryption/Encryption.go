package encryption

import "github.com/joomcode/errorx"

var (
	ErrorsEncryption       = errorx.NewNamespace("IEncryption")
	ErrorEncryptionEncrypt = errorx.NewType(ErrorsEncryption, "Encrypt")
	ErrorEncryptionDecrypt = errorx.NewType(ErrorsEncryption, "Decrypt")
)

type IEncryption interface {
	String() string
	Encrypt(data string, key string) (string, error)
	Decrypt(data string, key string) (string, error)
}
