package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
)

type AES struct {
}

func (A *AES) String() string {
	return "AES"
}

func (A *AES) Encrypt(data string, key string) (string, error) {
	blockCipher, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", ErrorEncryptionEncrypt.WrapWithNoMessage(err)
	}

	gcm, err := cipher.NewGCM(blockCipher)
	if err != nil {
		return "", ErrorEncryptionEncrypt.WrapWithNoMessage(err)
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = rand.Read(nonce); err != nil {
		return "", ErrorEncryptionEncrypt.WrapWithNoMessage(err)
	}

	ciphertext := gcm.Seal(nonce, nonce, []byte(data), nil)

	return string(ciphertext), nil
}

func (A *AES) Decrypt(data string, key string) (string, error) {
	blockCipher, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", ErrorEncryptionDecrypt.WrapWithNoMessage(err)
	}
	gcm, err := cipher.NewGCM(blockCipher)
	if err != nil {
		return "", ErrorEncryptionDecrypt.WrapWithNoMessage(err)
	}
	nonce, ciphertext := data[:gcm.NonceSize()], data[gcm.NonceSize():]
	plaintext, err := gcm.Open(nil, []byte(nonce), []byte(ciphertext), nil)
	if err != nil {
		return "", ErrorEncryptionDecrypt.WrapWithNoMessage(err)
	}
	return string(plaintext), nil
}
