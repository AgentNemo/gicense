# Gicense

Go library for generating license keys based on jwt tokens.

## Usage

### Without encryption

Generate a simple license in jwt token format with signing method HS256.
In this case secret is empty because no encryption was given.


````go
// Create a new Generator
licenseGenerator := NewLicenseGenerator()

// Generates a new license
license, err := licenseGenerator.GenerateLicense("Owner", "Customer", "CookiesGenerator", time.Now().Add(time.Minute), []byte("Password"))

// Print license key
fmt.Println(license.Key)

//Check if license is valid.
isValid, err := license.IsValid([]byte("Password"))

// Returns the payload of the license
payload, err := license.GetPayload([]byte("Password"))

// Returns the payload without validation
payload, err := license.GetPayloadWithoutValidation([]byte("Password"))
````

#### Custom Payload

You can also add custom data to your payload.

````go

// create payload
customPayload := NewPayload(licenseGenerator.generateID(), "Owner", "Customer", "CustomCookieMonster", time.Now().Add(time.Hour), map[string]interface{}{"version": "0.5.0"})	

licenseGenerator.GenerateLicenseFromPayload(customPaylaod, []byte("Password"))

````

### Encrytion

You can encrypt the token with any encryption method of your choice, implement the interface Encryption

The encryption secret is generated depending on the payload and time. The Secret can also be changed, implement the interface Secret

The default encryption is AES with a md5 hash secret.

````go
// Use dependecy injection to define the encryption
licenseOption := NewLicenseGenerator(DefaultEncryption)
````

If encryption is provided the generated license contains the secret and encryption.

````go
fmt.Println(license.Secret)
fmt.Println(license.Encryption)

````


## TODO

- Add tests
- Add all signing methods
- Add MD5, UUID and file format
- Create Rest API and CLI
