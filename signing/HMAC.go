package signing

import (
	"github.com/dgrijalva/jwt-go"
)

type HMAC struct {
	Signing Method
}

func NewHMAC(typ int) *HMAC {
	switch typ {
	case WEAK:
		return &HMAC{Signing: jwt.SigningMethodHS256}
	case MODERATE:
		return &HMAC{Signing: jwt.SigningMethodHS384}
	case STRONG:
		return &HMAC{Signing: jwt.SigningMethodHS512}
	default:
		return &HMAC{Signing: &jwt.SigningMethodHMAC{}}
	}
}

func (h *HMAC) Method() Method {
	return h.Signing
}


