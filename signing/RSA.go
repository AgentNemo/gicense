package signing

import "github.com/dgrijalva/jwt-go"

type RSA struct {
	Signing Method
}

func NewRSA(typ int) *RSA {
	switch typ {
	case WEAK:
		return &RSA{Signing: jwt.SigningMethodRS256}
	case MODERATE:
		return &RSA{Signing: jwt.SigningMethodRS384}
	case STRONG:
		return &RSA{Signing: jwt.SigningMethodRS512}
	default:
		return &RSA{Signing: &jwt.SigningMethodRSA{}}
	}
}

func (r *RSA) Method() Method {
	return r.Signing
}

