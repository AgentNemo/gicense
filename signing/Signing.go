package signing

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/joomcode/errorx"
)

const (
	WEAK = iota
	MODERATE
	STRONG
)

var (
	ErrorsSigning              = errorx.NewNamespace("ISigning")
	ErrorSigningMethodNotFound = errorx.NewType(ErrorsSigning, "Method not found")
)

type Method jwt.SigningMethod

type ISigning interface {
	Method() Method
}

func GetMethod(name string) Method {
	return jwt.GetSigningMethod(name)
}
