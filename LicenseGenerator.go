package gicense

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/AgentNemo/gicense/encryption"
	"gitlab.com/AgentNemo/gicense/secret"
	"gitlab.com/AgentNemo/gicense/signing"
	"time"
)

type Options func(option *LicenseGenerator)

type LicenseGenerator struct {
	Signing    signing.ISigning
	Secret     secret.ISecret
	Encryption encryption.IEncryption // optional
}

// Creates a new LicenseGenerator
func NewLicenseGenerator(options ...Options) *LicenseGenerator {
	licenseOption := LicenseGenerator{}
	licenseOption.defaultOptions()
	for _, option := range options {
		option(&licenseOption)
	}
	return &licenseOption
}

func (l *LicenseGenerator) defaultOptions() {
	DefaultSigning(l)
}

// Generates a encrypted License with a Secret
func (l *LicenseGenerator) GenerateLicense(from string, to string, product string, end time.Time, sign interface{}) (License, error) {
	payload := NewPayload(l.generateID(), from, to, product, end, nil)
	return l.GenerateLicenseFromPayload(*payload, sign)
}

// Generate a License from a Payload. Returns the License and a Secret needed to encrypt to License
func (l *LicenseGenerator) GenerateLicenseFromPayload(payload Payload, sign interface{}) (License, error) {
	err := payload.IsValid()
	if err != nil {
		return License{}, err
	}
	token := NewToken(payload, l.Signing)
	newSecret := l.generateSecret(payload)
	tokenString, err := token.Sign(sign)
	if err != nil {
		return License{}, err
	}
	license, err := l.encryptLicense(License{
		Key:        string(tokenString),
		Secret:     newSecret,
		Encryption: l.Encryption,
	})
	return license, err
}

// Generates a Secret depending on the given Payload and the current timestamp
func (l *LicenseGenerator) generateSecret(payload Payload) secret.Secret {
	if l.Secret == nil {
		return secret.Secret{}
	}
	return l.Secret.Generate(fmt.Sprintf("%v%s", payload, time.Now()))
}

// Encrypts the License with a Secret if the Encryption was set
func (l *LicenseGenerator) encryptLicense(license License) (License, error) {
	if l.Encryption == nil {
		return license, nil
	}
	encryptedData, err := l.Encryption.Encrypt(license.Key, license.Secret.Key)
	if err != nil {
		return license, err
	}
	return License{
		Key:        encryptedData,
		Secret:     license.Secret,
		Encryption: l.Encryption,
	}, nil
}

func (l *LicenseGenerator) generateID() string {
	id := uuid.New()
	return id.String()
}
