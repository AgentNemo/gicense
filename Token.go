package gicense

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/joomcode/errorx"
	"gitlab.com/AgentNemo/gicense/signing"
)

type TokenString string

var (
	ErrorsToken        = errorx.NewNamespace("Token")
	ErrorTokenNotValid = errorx.NewType(ErrorsToken, "NotValid")
	ErrorTokenSigning  = errorx.NewType(ErrorsToken, "ISigning")
)

type Token struct {
	*jwt.Token
}

// Creates a new Token
func NewToken(payload Payload, signing signing.ISigning) Token {
	return Token{jwt.NewWithClaims(signing.Method(), payload)}
}

// Signs the Token with a secret message
func (t *Token) Sign(sign interface{}) (TokenString, error) {
	tS, err := t.SignedString(sign)
	if err != nil {
		return "", ErrorTokenSigning.WrapWithNoMessage(err)
	}
	return TokenString(tS), nil
}

// Returns the tokens Payload
func (t *Token) GetPayload() (*Payload, error) {
	var payload Payload
	body, err := json.Marshal(t.Token.Claims.(jwt.MapClaims))
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &payload)
	return &payload, err
}
