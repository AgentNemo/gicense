package secret

type Secret struct {
	Key  string
	Type ISecret
}

type ISecret interface {
	String() string
	Generate(data string) Secret
}
