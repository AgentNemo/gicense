package secret

import (
	"crypto/md5"
	"fmt"
)

type Hash struct {
}

func NewHash() *Hash {
	return &Hash{}
}

func (h *Hash) String() string {
	return "MD5"
}

func (h *Hash) Generate(data string) Secret {
	return Secret{
		Key:  fmt.Sprintf("%x", md5.Sum([]byte(data))),
		Type: h,
	}
}
