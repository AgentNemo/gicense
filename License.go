package gicense

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/AgentNemo/gicense/encryption"
	"gitlab.com/AgentNemo/gicense/secret"
	"gitlab.com/AgentNemo/gicense/signing"
)

type License struct {
	Key        string
	Secret     secret.Secret
	Encryption encryption.IEncryption
}

// Checks if license is valid
func (l License) IsValid(verify interface{}) (bool, error) {
	payload, err := l.GetPayload(verify)
	if err != nil {
		return false, err
	}
	err = payload.IsValid()
	if err != nil {
		return false, err
	}
	return true, err
}

// Returns the Payload if valid
func (l License) GetPayload(verify interface{}) (*Payload, error) {
	license, err := l.Decrypt()
	token, err := license.parse(verify)
	if err != nil {
		return nil, err
	}
	payload, err := token.GetPayload()
	if err != nil {
		return payload, err
	}
	return payload, nil
}

// Returns the Payload, ignores validation errors
func (l License) GetPayloadWithoutValidation(verify interface{}) (*Payload, error) {
	license, err := l.Decrypt()
	token, err := license.parse(verify)
	if token == nil {
		return nil, err
	}
	payload, _ := token.GetPayload()
	if err != nil {
		return payload, err
	}
	return payload, nil
}

// Decrypts the License with a Secret. The Encryption type is needed
func (l License) Decrypt() (License, error) {
	if l.Encryption == nil {
		return l, nil
	}
	decryptedData, err := l.Encryption.Decrypt(l.Key, l.Secret.Key)
	if err != nil {
		return l, err
	}
	return License{
		Key:        decryptedData,
		Secret:     l.Secret,
		Encryption: l.Encryption,
	}, nil
}

// Parsers and creates a new Token
func (l License) parse(verify interface{}) (*Token, error) {
	token, err := jwt.Parse(l.Key, func(token *jwt.Token) (interface{}, error) {
		method := signing.GetMethod(token.Header["alg"].(string))
		if method == nil {
			return nil, signing.ErrorSigningMethodNotFound.New(fmt.Sprintf("method %s not found", token.Header["alg"]))
		}
		return verify, nil
	})
	if err != nil {
		return nil, err
	}
	return &Token{token}, err
}
